package com.hq.pager2video.model;

import lombok.Data;

@Data
public class VideoModel {
    private String uir;

    public VideoModel() {
    }

    public VideoModel(String uir) {
        this.uir = uir;
    }
}
