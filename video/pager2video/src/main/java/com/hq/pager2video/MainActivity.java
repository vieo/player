package com.hq.pager2video;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.hq.pager2video.adapter.ViewPagerAdapter;
import com.hq.pager2video.holder.RecyclerItemNormalHolder;
import com.hq.pager2video.model.VideoModel;
import com.shuyu.gsyvideoplayer.GSYVideoManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ViewPager2 viewPager2;
    private ViewPagerAdapter viewPagerAdapter;
    private List<VideoModel> dataList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
       this.viewPager2= findViewById(R.id.view_pager2);

       resolveData();
        // 1 创建视频滑动适配器
        viewPagerAdapter = new ViewPagerAdapter(this, dataList);
        this.viewPager2.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
       this.viewPager2.setAdapter(viewPagerAdapter);
      //注册滑动事件
       this.viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //大于0说明有播放
                int playPosition = GSYVideoManager.instance().getPlayPosition();
                if (playPosition >= 0) {
                    //对应的播放列表TAG
                    if (GSYVideoManager.instance().getPlayTag().equals(RecyclerItemNormalHolder.TAG)
                            && (position != playPosition)) {
                        GSYVideoManager.releaseAllVideos();
                        playPosition(position);
                    }
                }
            }
        });
        this.viewPager2.post(new Runnable() {
            @Override
            public void run() {
                playPosition(0);
            }
        });
    }
    @Override
    public void onBackPressed() {
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.onResume(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

    private void resolveData() {
        String host="https://m3u8.49cdn.com";
        String uir=host+"/newhd/202408/66cd4d62e2519513f3ea3340/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
         uir=host+"/newhd/202408/66cd4d62e2519513f3ea3343/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202408/66cd4d62e2519513f3ea3341/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202408/66cd4d62e2519513f3ea333e/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202301/63d7335299f1071ee6da98ba/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202408/66cd4d62e2519513f3ea3344/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202403/6606e32eee252d1bd907b4c5/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202403/6606e32eee252d1bd907b4f6/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202403/6606e32eee252d1bd907b4ee/hls/index.m3u8";
        dataList.add(new VideoModel(uir));
        uir=host+"/newhd/202407/669feb6de2519513f3e9ba92/hls/index.m3u8";
        dataList.add(new VideoModel(uir));




        if (viewPagerAdapter != null)
            viewPagerAdapter.notifyDataSetChanged();


    }


    private void playPosition(int position) {
        this.viewPager2.postDelayed(new Runnable() {
            @Override
            public void run() {
                RecyclerView.ViewHolder viewHolder = ((RecyclerView)
                        viewPager2.getChildAt(0)).findViewHolderForAdapterPosition(position);
                if (viewHolder != null) {
                    RecyclerItemNormalHolder recyclerItemNormalHolder = (RecyclerItemNormalHolder) viewHolder;
                    recyclerItemNormalHolder.getPlayer().startPlayLogic();
                }
            }
        }, 50);
    }

}