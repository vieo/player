package com.hq.play.util;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.hq.play.base.data.VideoEnityData;

/**
 * 视频数据对象重复判断
 * 用于分页加载适配器 防止对象重复
 */
public class VideoEnityDataDiffUtil extends DiffUtil.ItemCallback<VideoEnityData> {
    @Override
    public boolean areItemsTheSame(@NonNull VideoEnityData oldItem, @NonNull VideoEnityData newItem) {
        return oldItem.getId().equals(newItem.getId());
    }

    @Override
    public boolean areContentsTheSame(@NonNull VideoEnityData oldItem, @NonNull VideoEnityData newItem) {
        return oldItem.equals(newItem);
    }
}
