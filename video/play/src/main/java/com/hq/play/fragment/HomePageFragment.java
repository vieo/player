package com.hq.play.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.hq.play.adapter.HomePageAdapter;
import com.hq.play.databinding.FragmentHomePageBinding;
import com.hq.play.view.model.PagingModel;


/**
 *首页展示页面入口
 */
public class HomePageFragment extends Fragment {
    private FragmentHomePageBinding binding;

    private PagingModel viewModel;
    private HomePageAdapter homePageAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.homePageAdapter = new HomePageAdapter();
        // 添加布局风格
        this.binding.videoRecycer.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        this.binding.videoRecycer.setAdapter(homePageAdapter);
        this.viewModel = new ViewModelProvider(requireActivity()).get(PagingModel.class);
        //注册页面加载绑定数据
        this.viewModel.getData().observe(this.getViewLifecycleOwner(), dataPagingData -> homePageAdapter.submitData(getLifecycle(), dataPagingData));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentHomePageBinding.inflate(inflater, container, false);
        // View view = binding.getRoot();


        return binding.getRoot();
    }
}