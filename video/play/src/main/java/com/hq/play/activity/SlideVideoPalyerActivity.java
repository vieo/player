package com.hq.play.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.hq.play.adapter.SlideVideoPageAdapter;
import com.hq.play.databinding.ActivitySlideVideoPalyerBinding;
import com.hq.play.view.model.PagingModel;
import com.shuyu.gsyvideoplayer.GSYVideoManager;

public class SlideVideoPalyerActivity extends AppCompatActivity {

    private PagingModel viewModel;
    private ActivitySlideVideoPalyerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySlideVideoPalyerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // 1 创建视频滑动适配器
        SlideVideoPageAdapter adapter = new SlideVideoPageAdapter();
        // 2 添加适配器
        this.binding.palyerPager2.setAdapter(adapter);
        // 3 设置 垂直方向
        this.binding.palyerPager2.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
        // 4 获取 viewModel
        this.viewModel = new ViewModelProvider(this).get(PagingModel.class);
        // 5 注册页面加载绑定数据
        this.viewModel.getData().observe(this, dataPagingData -> adapter.submitData(getLifecycle(), dataPagingData));
        // 6 注册滑动事件
        this.binding.palyerPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //大于0说明有播放
                int playPosition = GSYVideoManager.instance().getPlayPosition();
                if (playPosition >= 0) {
                    //对应的播放列表TAG
                    if (GSYVideoManager.instance().getPlayTag().equals(SlideVideoPageAdapter.Holder.TAG)
                            && (position != playPosition)) {
                        GSYVideoManager.releaseAllVideos();
                        playPosition(position);
                    }
                }
            }
        });


    }

    /**
     * 用户滑动到页面条目调用 播放视频
     *
     * @param position 视频条目索引
     */
    private void playPosition(int position) {
        this.binding.palyerPager2.postDelayed(new Runnable() {
            @Override
            public void run() {
                RecyclerView.ViewHolder viewHolder = ((RecyclerView)
                        binding.palyerPager2.getChildAt(0)).findViewHolderForAdapterPosition(position);
                if (viewHolder != null) {
                    SlideVideoPageAdapter.Holder recyclerItemNormalHolder = (SlideVideoPageAdapter.Holder) viewHolder;
                    recyclerItemNormalHolder.getPlayer().startPlayLogic();
                }
            }
        }, 50);
    }

    @Override
    public void onStart() {
        super.onStart();
        this.binding.palyerPager2.post(new Runnable() {
            @Override
            public void run() {
                playPosition(0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.onResume(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 获取FragmentManager
        FragmentManager fragmentManager = getSupportFragmentManager();
        //  fragmentManager.popBackStack();


    }
}