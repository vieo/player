package com.hq.play.base.data;

import com.alibaba.fastjson2.JSON;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import lombok.Data;

/**
 * 网络请求数据结果集
 * 封装实体类
 */
@Data
public class Result implements JsonDeserializer<Result>{
    private Integer code;
    private String msg;
    private PageData data;

    @Override
    public Result deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String jsonRoot = json.getAsJsonObject().toString() ;
        return  JSON.parseObject(jsonRoot, Result.class);
    }


}

