package com.hq.play.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.paging.ListenableFuturePagingSource;
import androidx.paging.PagingState;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.hq.play.base.data.PageData;
import com.hq.play.base.data.VideoEnityData;
import com.hq.play.util.IhttpService;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * 分页 数据源配置
 * 泛型 Records ：数据实体类
 * 泛型 Integer ：分页页码
 */
public class VideoBaseDataSourceAll extends ListenableFuturePagingSource<Integer, VideoEnityData> {
    private final IhttpService api;//获取网络接口数据
    private final ListeningExecutorService pool;//线程池

    public VideoBaseDataSourceAll(IhttpService api, ListeningExecutorService pool) {
        this.api = api;
        this.pool = pool;
    }


    @Nullable
    @Override
    public Integer getRefreshKey(@NonNull PagingState<Integer, VideoEnityData> pagingState) {
        Integer anchorPosition = pagingState.getAnchorPosition();
        if (anchorPosition == null) {
            return null;
        }

        LoadResult.Page<Integer, VideoEnityData> anchorPage = pagingState.closestPageToPosition(anchorPosition);
        if (anchorPage == null) {
            return null;
        }

        Integer prevKey = anchorPage.getPrevKey();
        if (prevKey != null) {
            return prevKey + 1;
        }

        Integer nextKey = anchorPage.getNextKey();
        if (nextKey != null) {
            return nextKey - 1;
        }

        return null;
    }

    /**
     * 加载数据
     *
     * @param loadParams
     * @return 分页数据
     */
    @NonNull
    @Override
    public ListenableFuture<LoadResult<Integer, VideoEnityData>> loadFuture(@NonNull LoadParams<Integer> loadParams) {
        Integer next = loadParams.getKey();
        //第一页
        if (next == null) {
            next = 1;
        } else {
            next = loadParams.getKey() + 1;
        }
        ListenableFuture<LoadResult<Integer, VideoEnityData>> pageFuture = Futures.transform(api.ResultPagVideo(next, PageData.pageSize), this::toLoadResult, pool);
        ListenableFuture<LoadResult<Integer, VideoEnityData>> partialLoadResultFuture = Futures.catching(pageFuture, HttpException.class, LoadResult.Error::new, this.pool);

        return Futures.catching(partialLoadResultFuture, IOException.class, LoadResult.Error::new, this.pool);
    }

    private LoadResult<Integer, VideoEnityData> toLoadResult(@NonNull PageData response) {
        Integer nextPage = null;//下一页
        if (response.getPageNum() < response.getPages()) {
            nextPage = response.getPageNum();
        }
        return new LoadResult.Page<>(response.getRecords(), null, nextPage, LoadResult.Page.COUNT_UNDEFINED, LoadResult.Page.COUNT_UNDEFINED);
    }
}
