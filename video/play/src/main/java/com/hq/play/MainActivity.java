package com.hq.play;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationBarView;
import com.hq.play.adapter.FmentPageAdapter;
import com.hq.play.databinding.ActivityMainBinding;
public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //视图绑定
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = this.binding.getRoot();
        setContentView(view);
        FmentPageAdapter adapter = new FmentPageAdapter(getSupportFragmentManager(), getLifecycle());
        binding.homePager.setAdapter(adapter);
        // 创建并注册页面改变的回调
        ViewPager2.OnPageChangeCallback onp = new ViewPager2.OnPageChangeCallback() {

            @Override
            public void onPageSelected(int position) {
                // 用户选择了一个新页面，position是新页面的位置
                //设置底部菜单选中 //当viewpage2页面切换时nav导航图标也跟着切换
                binding.tabs.getMenu().getItem(position).setChecked(true);
                if (position == 2) {
                    //隐藏菜单
                    binding.tabs.setVisibility(View.GONE);
                } else {
                     //隐藏菜单
                    binding.tabs.setVisibility(View.VISIBLE);
                }

            }

        };
        //注册滑动回调
        binding.homePager.registerOnPageChangeCallback(onp);
        // 注册底部菜单点击事件
        binding.tabs.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.menu_home) {
                    binding.homePager.setCurrentItem(0);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_video) {
                    binding.homePager.setCurrentItem(1);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_lisi) {
                    binding.homePager.setCurrentItem(2);
                    //隐藏菜单
                    binding.tabs.setVisibility(View.GONE);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_gern) {
                    binding.homePager.setCurrentItem(3);
                    return true;
                }

                return true;

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}