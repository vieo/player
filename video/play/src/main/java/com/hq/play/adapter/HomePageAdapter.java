package com.hq.play.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hq.play.R;
import com.hq.play.base.data.VideoEnityData;
import com.hq.play.util.VideoEnityDataDiffUtil;

/**
 * 首页 适配器
 * 分页适配器
 */
public class HomePageAdapter extends PagingDataAdapter<VideoEnityData, HomePageAdapter.Holder> {

    public HomePageAdapter() {
        super(new VideoEnityDataDiffUtil());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_enity_data_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        //绑定数据
        VideoEnityData bean=getItem(position);
        holder.update(bean);
    }

    /**
     * 视频页面展示页面
     * 页面适配器
     */
    public static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView im = null;
        private TextView name = null;
        private TextView group = null;
        private TextView temp = null;
        private TextView uptemp = null;
        //图片加载器
        private RequestManager with = null;
        public Holder(@NonNull View itemView) {
            super(itemView);
            this.im = itemView.findViewById(R.id.im);
            this.name = itemView.findViewById(R.id.name);
            this.group = itemView.findViewById(R.id.group);
            this.temp = itemView.findViewById(R.id.play_temp);
            this.uptemp = itemView.findViewById(R.id.uptemp);
            this.with = Glide.with(itemView);
            // 设置TextView为选中状态以开启跑马灯效果
            this.name.setSelected(true);
            //注册信息
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

        }

        /**
         * 数据更新
         * @param bean 数据对象
         */
        public void update(VideoEnityData bean) {
            //图片更新
            String gif = "https://5b0988e595225.cdn.sohucs.com/images/20190721/67d2edceaecf4fa690c9a37fe37ddb40.gif";
            //图片显示
            this.with.load(gif).into(this.im);
            //视频昵称
            this.name.setText(bean.getName());
            // 设置TextView为选中状态以开启跑马灯效果
            this.name.setSelected(true);
            //视频分组
            this.group.setText(bean.getMenuName());
            //视频时长
            //  this.temp.setText(this.date.getPlayitmp().toString());
            //视频更新时间
            // this.uptemp.setText(this.date.getUptime().toString());
        }
    }



}
