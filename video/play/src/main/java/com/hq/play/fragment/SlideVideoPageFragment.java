package com.hq.play.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.hq.play.adapter.SlideVideoPageAdapter;
import com.hq.play.databinding.FragmentSlideVideoPageBinding;
import com.hq.play.view.model.PagingModel;
import com.shuyu.gsyvideoplayer.GSYVideoManager;


/**
 * 刷视频页面展示 Fragment 页面入口
 */
public class SlideVideoPageFragment extends Fragment {
    private FragmentSlideVideoPageBinding binding;
    private PagingModel viewModel;
    private SlideVideoPageAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewModel = new ViewModelProvider(requireActivity()).get(PagingModel.class);
        // 1 创建视频滑动适配器
        this.adapter = new SlideVideoPageAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = FragmentSlideVideoPageBinding.inflate(inflater, container, false);
        return this.binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        // 2 添加适配器
        this.binding.palyerPager2.setAdapter(this.adapter);
        // 3 设置 垂直方向
        this.binding.palyerPager2.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
        // 5 注册页面加载绑定数据
        this.viewModel.getData().observe(this.getViewLifecycleOwner(), dataPagingData -> this.adapter.submitData(getLifecycle(), dataPagingData));
        // 6 注册滑动事件
        this.binding.palyerPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //大于0说明有播放
                int playPosition = GSYVideoManager.instance().getPlayPosition();
                if (playPosition >= 0) {
                    //对应的播放列表TAG
                    if (GSYVideoManager.instance().getPlayTag().equals(SlideVideoPageAdapter.Holder.TAG)
                            && (position != playPosition)) {
                        GSYVideoManager.releaseAllVideos();
                        playPosition(position);
                    }
                }
            }
        });

        this.binding.palyerPager2.post(new Runnable() {
            @Override
            public void run() {
                playPosition(0);
            }
        });


    }

    /**
     * 用户滑动到页面条目调用 播放视频
     *
     * @param position 视频条目索引
     */
    private void playPosition(int position) {
        this.binding.palyerPager2.postDelayed(new Runnable() {
            @Override
            public void run() {
                RecyclerView.ViewHolder viewHolder = ((RecyclerView)
                        binding.palyerPager2.getChildAt(0)).findViewHolderForAdapterPosition(position);
                if (viewHolder != null) {
                    SlideVideoPageAdapter.Holder recyclerItemNormalHolder = (SlideVideoPageAdapter.Holder) viewHolder;
                    recyclerItemNormalHolder.getPlayer().startPlayLogic();
                }
            }
        }, 50);
    }

    @Override
    public void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        GSYVideoManager.onResume(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

}