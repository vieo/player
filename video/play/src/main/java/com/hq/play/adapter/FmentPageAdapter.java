package com.hq.play.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.hq.play.fragment.HomePageFragment;
import com.hq.play.fragment.MenuPageFragment;
import com.hq.play.fragment.SlideVideoPageFragment;
import com.hq.play.fragment.UserPageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment 页面适配器
 */
public class FmentPageAdapter extends FragmentStateAdapter {
    /**
     * 页面集合
     */
    private List<Fragment> page;
    public FmentPageAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
        this.page=new ArrayList<>();
       //添加页面
        this.page.add(new HomePageFragment());
        this.page.add(new MenuPageFragment());
        this.page.add(new SlideVideoPageFragment());
        this.page.add(new UserPageFragment());

    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return this.page.get(position);
    }

    @Override
    public int getItemCount() {
        return this.page.size();
    }
}
