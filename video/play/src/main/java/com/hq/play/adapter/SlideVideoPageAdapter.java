package com.hq.play.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.hq.play.R;
import com.hq.play.base.data.VideoEnityData;
import com.hq.play.util.VideoEnityDataDiffUtil;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;

import lombok.Getter;

/**
 * 滑动视频适配器
 */
public class SlideVideoPageAdapter extends PagingDataAdapter<VideoEnityData, SlideVideoPageAdapter.Holder> {
    public SlideVideoPageAdapter() {
        super(new VideoEnityDataDiffUtil());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.slide_video_player_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        VideoEnityData item = getItem(position);
        //数据绑定
        holder.upData(position, item);
    }

    /**
     * 滑动视频页面适配器
     */
    public static class Holder extends RecyclerView.ViewHolder {
        public final static String TAG = "SeeVideoPageAdapterHolder";
        private GSYVideoOptionBuilder gsyVideoOptionBuilder;
        @Getter
        private StandardGSYVideoPlayer player;

        public Holder(@NonNull View v) {
            super(v);
            //播发器配置工具类
            this.gsyVideoOptionBuilder = new GSYVideoOptionBuilder();
            //播发器
            this.player = v.findViewById(R.id.see_player_item);
        }

        //数据绑定
        public void upData(int position, VideoEnityData item) {
            String uir = "https://m3u8.49cdn.com" + item.getPlaym3u8();
            String mp4uir=item.getHttpmp4();
            //防止错位，离开释放
            this.player.initUIState();
            this.gsyVideoOptionBuilder
                    .setIsTouchWiget(false)
                    .setUrl(mp4uir)
                    .setVideoTitle(item.getName())
                    .setCacheWithPlay(false)
                    .setRotateViewAuto(true)
                    .setLockLand(true)
                    .setPlayTag(TAG)
                    //.setMapHeadData(header)
                    .setShowFullAnimation(true)
                    .setNeedLockFull(false)
                    .setPlayPosition(position)
                    .setVideoAllCallBack(new GSYSampleCallBack() {
                        @Override
                        public void onPrepared(String url, Object... objects) {
                            super.onPrepared(url, objects);
                            if (!player.isIfCurrentIsFullscreen()) {
                                //静音
                                //   GSYVideoManager.instance().setNeedMute(true);
                            }

                        }

                        @Override
                        public void onQuitFullscreen(String url, Object... objects) {
                            super.onQuitFullscreen(url, objects);
                            //全屏不静音
                            GSYVideoManager.instance().setNeedMute(true);
                        }

                        @Override
                        public void onEnterFullscreen(String url, Object... objects) {
                            super.onEnterFullscreen(url, objects);
                            GSYVideoManager.instance().setNeedMute(false);
                            player.getCurrentPlayer().getTitleTextView().setText((String) objects[0]);
                        }
                    }).build(player);

            //隐藏播放器控件按钮
            //增加title
            //this.player.getTitleTextView().setVisibility(View.GONE);

            //设置返回键
            this.player.getBackButton().setVisibility(View.GONE);
            //设置全屏按键功能
            this.player.getFullscreenButton().setVisibility(View.GONE);

        }
    }
}
