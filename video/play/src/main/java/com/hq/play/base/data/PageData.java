package com.hq.play.base.data;

import java.util.List;

import lombok.Data;

/**
 * 视频分页数据封装类
 * 网络视频 分页数据封装
 *
 *
 *
 *
 *
 */
@Data
public class PageData {
    //定义每次加载更多数据时应该获取的数据量
    public static final int pageSize=20;
    private Integer total;
    private Integer size;
    private Integer current;
    private Integer pages;//当前分页总页数
    private int pageNum;//当前页码
    private List<VideoEnityData> records;
}
