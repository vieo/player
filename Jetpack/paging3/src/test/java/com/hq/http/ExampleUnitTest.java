package com.hq.http;

import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    void getBody() throws IOException {
        HttpClient http=new HttpClient();

        IhttpApi api = http.createApi(IhttpApi.class, "http://192.168.2.253/");

        Result r = api.pagVideo(1,50).execute().body();
        List<Records> re = r.getData().getRecords();
        for (Records record : re) {
            System.out.println(record.getName());
        }
        System.out.println(r.getMsg());
    }

}