package com.hq.http;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.CombinedLoadStates;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
      //绑定数据
        RecyclerView rv=findViewById(R.id.video_recycer);
        PagingAdapter ra=new PagingAdapter(new PagingAdapter.Diff());
        // 添加布局风格
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        rv.setAdapter(ra);
        ra.withLoadStateFooter(new ExampleLoadStateAdapter());

        ra.addLoadStateListener(new Function1<CombinedLoadStates, Unit>() {
            @Override
            public Unit invoke(CombinedLoadStates combinedLoadStates) {
                // 非加载中
                //数据加载错误
                if (combinedLoadStates.getPrepend() instanceof LoadState.NotLoading) {
                    Log.d("HttpSevvice","非加载中");

                } if (combinedLoadStates.getPrepend() instanceof LoadState.Loading) {
                    Log.d("HttpSevvice","加载中");

                }
                 if (combinedLoadStates.getPrepend() instanceof LoadState.Error) {
                    Log.d("HttpSevvice","加载错误");

                    // this.btn_retry.setVisibility(View.VISIBLE);
                }
                return null;
            }
        });
        // 设置加载状态监听





        //注册数据加载
        PagingModel viewModel=new ViewModelProvider(this).get(PagingModel.class);
        viewModel.getData().observe(this, dataPagingData -> ra.submitData(getLifecycle(),dataPagingData));

    }
}