package com.hq.http;

import com.alibaba.fastjson2.JSON;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.List;

/**
 * 网络请求数据结果集
 */
@Data
public class Result implements JsonDeserializer<Result>{
    private Integer code;
    private String msg;
    private PagData data;

    @Override
    public Result deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String jsonRoot = json.getAsJsonObject().toString() ;
        return  JSON.parseObject(jsonRoot, Result.class);
    }

    @Data
    public static class PagData{
        private Integer total;
        private Integer size;
        private Integer current;
        private Integer pages;//当前分页总页数
        private int pageNum;//当前页码
        private List<Records> records;
    }


}

