package com.hq.http;

import com.google.common.util.concurrent.ListenableFuture;

import retrofit2.http.Path;

/**
 * 联网服务接口用于获取网络数据
 */
public interface IhttpService {
    /**
     * 分页关联查询所有视频信息
     * 分组类型 分组位置 视频标题 视频简介 视频封面 视频地址 视频时长
     * 排序根据视频更新时间升序
     * @param pageNum  当前页码
     * @param pageSize 每页显示条数
     * @return 分页结果
     */
    ListenableFuture<Result.PagData> ResultPagVideo(int pageNum, int pageSize);
}
