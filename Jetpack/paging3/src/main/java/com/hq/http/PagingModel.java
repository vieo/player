package com.hq.http;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.Executors;

import kotlinx.coroutines.CoroutineScope;
import lombok.Getter;

public class PagingModel  extends ViewModel {
   private ListeningExecutorService pool;//线程池
   private PagingConfig config;//初始化配置
    private int InitialLoadSize=100;//：定义初始加载时应该获取的数据量。
    private int BoundaryCallback;//‌：用于处理数据加载的边界情况，如到达数据源的末尾或数据加载

    @Getter
    private LiveData<PagingData<Records>> data;
    private IhttpService httpService;//联网服务接口
    private BaseDataSource baseData;//数据原
    public PagingModel() {
        // 初始化服务
        this.config=new PagingConfig(Records.pageSize,20,false,Records.pageSize);
        this.pool=MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        this.httpService=new HttpSevvice(this.pool);
        this.baseData=new BaseDataSource(this.httpService,this.pool);
        CoroutineScope viewmodelScope= ViewModelKt.getViewModelScope(this);
        Pager<Integer, Records> pager = new Pager<Integer,Records>(this.config, ()->this.baseData);
        this.data= PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager),viewmodelScope);
    }

}
