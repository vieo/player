package com.hq.http;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IhttpApi {
    /**
     *      * 分页关联查询所有视频信息
     *      * 分组类型 分组位置 视频标题 视频简介 视频封面 视频地址 视频时长
     *      * 排序根据视频更新时间升序
     *      * @param pageNum  当前页码
     *      * @param pageSize 每页显示条数
     */
    @GET("videoMessage/index&pageNum={pageNum}&&pageSize{pageSize}")
    Call<Result> pagVideo(@Path("pageNum")int pageNum,@Path("pageSize")int pageSize);
}
