package com.hq.http;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import kotlin.coroutines.CoroutineContext;

/**
 * 分页适配器
 */
public class PagingAdapter  extends PagingDataAdapter<Records, PagingAdapter.Holder> {
    public PagingAdapter(@NonNull DiffUtil.ItemCallback<Records> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item_message, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        //绑定数据
        Records bean=getItem(position);
        holder.update(bean);
    }

    /**
     * 视频页面展示页面
     * 页面适配器
     */
    public static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView im = null;
        private TextView name = null;
        private TextView group = null;
        private TextView temp = null;
        private TextView uptemp = null;
        private Records date = null;
        //图片加载器
        private RequestManager with = null;
        public Holder(@NonNull View itemView) {
            super(itemView);
            this.im = itemView.findViewById(R.id.im);
            this.name = itemView.findViewById(R.id.name);
            this.group = itemView.findViewById(R.id.group);
            this.temp = itemView.findViewById(R.id.play_temp);
            this.uptemp = itemView.findViewById(R.id.uptemp);
            this.with = Glide.with(itemView);
            // 设置TextView为选中状态以开启跑马灯效果
            this.name.setSelected(true);
            //注册信息
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
        /**
         * 更新数据
         *
         * @param date 视频数据
         */
        public void update(Records date) {
            //图片更新
            String gif = "https://5b0988e595225.cdn.sohucs.com/images/20190721/67d2edceaecf4fa690c9a37fe37ddb40.gif";
            this.date = date;
            //图片显示
            this.with.load(gif).into(this.im);
            //视频昵称
            this.name.setText(this.date.getName());
            // 设置TextView为选中状态以开启跑马灯效果
            this.name.setSelected(true);
            /**
             * //设置TextView只显示一行文本。
             * tv2.setSingleLine();
             * //设置TextView的文本内容是否可以水平滚动。
             * tv2.setHorizontallyScrolling(true);
             * //设置当TextView的文本内容超出可显示范围时的省略方式，这里设置为跑马灯效果。
             * tv2.setEllipsize(TextUtils.TruncateAt.MARQUEE);
             * //设置跑马灯效果重复的次数，-1表示无限重复。
             * tv2.setMarqueeRepeatLimit(-1);
             * //设置TextView是否可以获取焦点。
             * tv2.setFocusable(true);
             * //设置TextView在触摸模式下是否可以获取焦点。
             * tv2.setFocusableInTouchMode(true);
             * //请求获取焦点。
             * tv2.requestFocus();
             */
            //视频分组
            this.group.setText(this.date.getMenuName());
            //视频时长
          //  this.temp.setText(this.date.getPlayitmp().toString());
            //视频更新时间
           // this.uptemp.setText(this.date.getUptime().toString());
        }

    }

    /**
     * 对象判断
     */
    public static class Diff extends DiffUtil.ItemCallback<Records>{
        @Override
        public boolean areItemsTheSame(@NonNull Records oldItem, @NonNull Records newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Records oldItem, @NonNull Records newItem) {
            return oldItem.equals(newItem);
        }

    }

}
