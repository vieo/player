package com.hq.http;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 视频查询结果实体类
 */
@Data
public class Records {

    public static final int pageSize=20;//‌：定义每次加载更多数据时应该获取的数据量
    private String groupName;//分组名称
    private String menuName;//分组条目昵称
    /**
     * 主键自动增加id
     */
    private Integer id;

    /**
     * 关联视频分组
     */
    private Integer groupId;

    /**
     * 视频名称
     */
    private String name;

    /**
     * 视频更新时间
     */
    private LocalDate uptime;

    /**
     * 视频播放时间
     */
    private LocalTime playitmp;

    /**
     * 播放次数
     */
    private Integer playcont;
    /**
     * 图片地址
     */
    private String imagepath;
    /**
     * html网页地址
     */
    private String htmlweb;

    /**
     * mp4下载地址
     */
    private String httpmp4;
    /**
     * m3u8地址 播放地址
     */
    private String playm3u8;

}
