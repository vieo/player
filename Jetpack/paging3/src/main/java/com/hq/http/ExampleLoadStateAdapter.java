package com.hq.http;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.LoadState;
import androidx.paging.LoadStateAdapter;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 数据加载状态适配器管理
 */
public class ExampleLoadStateAdapter extends LoadStateAdapter<ExampleLoadStateAdapter.LHolder> {
    @Override
    public void onBindViewHolder(@NonNull LHolder holder, @NonNull LoadState loadState) {
        holder.bind(loadState);
    }

    @NonNull
    @Override
    public LHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, @NonNull LoadState loadState) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.load_state_view_holder, viewGroup, false);
        return new LHolder((itemView));
    }
    /**
     * 数据加载页面适配器
     */
    public static class LHolder  extends RecyclerView.ViewHolder{
        private android.widget.ProgressBar ProgressBar;
        private TextView tv_load;
        private TextView tv_all_message;
        private TextView btn_retry;


        public LHolder(@NonNull View v) {
            super(v);
            this.ProgressBar= v.findViewById(R.id.progressBar);
            this.tv_load= v.findViewById(R.id.tv_load);
            this.tv_all_message= v.findViewById(R.id.tv_all_message);
            this.btn_retry= v.findViewById(R.id.btn_retry);

        }

        public void bind(LoadState loadState) {
            // 非加载中
            //数据加载错误
            if (loadState instanceof LoadState.NotLoading) {
                Log.d("HttpSevvice","非加载中");
                this.ProgressBar.setVisibility(View.GONE);
                this.tv_load.setVisibility(View.GONE);
                this.tv_all_message.setVisibility(View.GONE);
                this.btn_retry.setVisibility(View.GONE);
            }else if (loadState instanceof LoadState.Loading) {
                Log.d("HttpSevvice","加载中");

            }
            else if (loadState instanceof LoadState.Error) {
                Log.d("HttpSevvice","加载错误");

                // this.btn_retry.setVisibility(View.VISIBLE);
            }

        }

    }

}
