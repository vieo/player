package com.hq.viewpager2_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hq.viewpager2_fragment.databinding.FragmentMainBinding;

public class PlaceholderFragment  extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private FragmentMainBinding binding;
    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SECTION_NUMBER,"顶顶顶顶"+ index);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        String s = getArguments().getString(ARG_SECTION_NUMBER);
        binding.fragmentText.setText(s);
        return root;
    }
}
