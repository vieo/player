package com.hq.viewpager2_fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.hq.viewpager2_fragment.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //视图绑定
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = this.binding.getRoot();
        setContentView(view);


        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager(), getLifecycle());
        binding.homePager.setAdapter(adapter);

        // 创建并注册页面改变的回调
        ViewPager2.OnPageChangeCallback onp = new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // 页面正在滑动，可以获取到滑动的位置信息
            }

            @Override
            public void onPageSelected(int position) {
                // 用户选择了一个新页面，position是新页面的位置
                //设置底部菜单选中 //当viewpage2页面切换时nav导航图标也跟着切换
                binding.tabs.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // 页面滑动状态改变，例如开始，结束，正在滑动
            }
        };
        //注册滑动回调
        binding.homePager.registerOnPageChangeCallback(onp);

        // 注册底部菜单点击事件
        binding.tabs.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.menu_home) {
                    binding.homePager.setCurrentItem(0);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_video) {
                    binding.homePager.setCurrentItem(1);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_lisi) {
                    binding.homePager.setCurrentItem(2);
                    return true;
                }

                if (menuItem.getItemId() == R.id.menu_gern) {
                    binding.homePager.setCurrentItem(3);
                    return true;
                }

                return true;

            }
        });


    }
}