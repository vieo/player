# 依赖 
dependencies {
def paging_version = "3.3.2"

implementation "androidx.paging:paging-runtime:$paging_version"

// alternatively - without Android dependencies for tests
testImplementation "androidx.paging:paging-common:$paging_version"

// optional - RxJava2 support
implementation "androidx.paging:paging-rxjava2:$paging_version"

// optional - RxJava3 support
implementation "androidx.paging:paging-rxjava3:$paging_version"

// optional - Guava ListenableFuture support
implementation "androidx.paging:paging-guava:$paging_version"

// optional - Jetpack Compose integration
implementation "androidx.paging:paging-compose:3.3.2"
}
# 1 创建 数据源
public class BaseDataSource extends ListenableFuturePagingSource<Integer,Records> 